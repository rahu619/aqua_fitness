package com.example.rahul.aqua_dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;

import java.util.List;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ScheduleDisplay();
    }

    public void dryAction(View v){
        Intent _intent=new Intent(HomeActivity.this,DryClassActivity.class);
        startActivity(_intent);

    }

    public void wetAction(View v){
        Intent _intent=new Intent(HomeActivity.this,RowActivity.class);
        startActivity(_intent);

    }


    public void overallAction(View v){
        Intent _intent=new Intent(HomeActivity.this,OverallActivity.class);
        startActivity(_intent);

    }


    public void monthAction(View v){
        Intent _intent=new Intent(HomeActivity.this,MonthlyActivity.class);
        startActivity(_intent);
    }




    private void ScheduleDisplay(){
       String _class= Data.NextClassSchedule();
       List<SingleSeries> _enrollments= Data.MemberCount();

       TextView tv= findViewById(R.id.txtClass);
       TextView dry=findViewById(R.id.tvmembers);
       TextView wet=findViewById(R.id.tvwetmembers);


       //Session details
       int initialLength=tv.getText().length()+2;
        _class=tv.getText()+" : "+_class;

        Spannable spannable = new SpannableString(_class);
        spannable.setSpan(new ForegroundColorSpan(Color.parseColor("#ad84ff")),initialLength,_class.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setText(spannable,TextView.BufferType.SPANNABLE);


        //Count
        wet.setText((int)_enrollments.get(0).get_data()+" entries");
        dry.setText((int)_enrollments.get(1).get_data()+" entries");



    }
}
