package com.example.rahul.aqua_dashboard;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class MonthlyActivity extends AppCompatActivity {


    Button _month;
    String[] _listitems;
    boolean[] _checkeditems;
    List<String> _selectedMonths=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_monthly);

        _month=findViewById(R.id.btnMonth);
        _listitems =getResources().getStringArray(R.array.Months);
        _checkeditems=new boolean[_listitems.length];
        _month.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v){
                AlertDialog.Builder _builder=new AlertDialog.Builder(MonthlyActivity.this);
                _builder.setTitle("Choose months");
                _builder.setMultiChoiceItems(_listitems, _checkeditems, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position, boolean isChecked) {

                        int month=position+1;

                        if(!isChecked)
                            _selectedMonths.remove(String.valueOf(month));

                        else if(!_selectedMonths.contains(String.valueOf(month)))
                                _selectedMonths.add(String.valueOf(month));


                    }
                });

                _builder.setCancelable(false);
                _builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        createMonthlyReport();

                    }
                });

                _builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        dialog.dismiss();
                    }
                });

                _builder.setNeutralButton("Clear All", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        for(int i=0;i<_checkeditems.length;i++)
                            _checkeditems[i]=false;

                        _selectedMonths.clear();
                        createMonthlyReport();
                    }
                });

                AlertDialog _dialog= _builder.create();
                _dialog.show();


            }

        });

        createMonthlyReport();
    }



    public void createMonthlyReport(){

        Log.i("OAM","Entered CreateMonthlyReport");
        CombinedChart _cc= findViewById(R.id.ccMonthly);
        _cc.setDrawGridBackground(false);
        _cc.setDrawBarShadow(false);

        _cc.getDescription().setEnabled(false);

        // draw bars behind lines
        _cc.setDrawOrder(new CombinedChart.DrawOrder[]{
                CombinedChart.DrawOrder.BAR, CombinedChart.DrawOrder.LINE
        });

       // _cc.setOnChartValueSelectedListener(this);
        YAxis rightAxis = _cc.getAxisRight();
        rightAxis.setDrawGridLines(false);


        YAxis leftAxis = _cc.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setTextColor(Color.LTGRAY);


        //Data
        List<MultipleSeries> _data = Data.LoadMonthlyReport(_selectedMonths);

        //For Dry and Aqua classes
        List<SingleSeries> _first= _data.stream().map(f->f.get_singleSeries()).findFirst().get();
        List<SingleSeries> _second= _data.stream().map(f->f.get_singleSeries()).reduce((f,s)->s).get();


        //Bardata properties
        BarData bd=new BarData(GenerateBarDataSet(_first,true),GenerateBarDataSet(_second,false));
        bd.groupBars(0,0.3f,0f);
        bd.setBarWidth(1f);

        CombinedData data = new CombinedData();

        //x-axis months
        List<String> xVals = _first.stream().map(x->x.get_label().substring(0,3)).collect(Collectors.toList());

       //Linedata
        LineData ld=new LineData(GenerateLineDataSet(getAverage(_first,_second)));
        data.setData(ld);
        data.setData(bd);
        _cc.setData(data);

        //x-axis properties
        XAxis xAxis = _cc.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
       // xAxis.setAxisMinimum(0);    //    xAxis.setAxisMaximum(_cc.getBarData().getGroupWidth(2, 2) * xVals.size());
        //xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(1f);
        xAxis.setDrawGridLines(false);
        xAxis.setTextSize(8);
       // xAxis.setLabelCount(xVals.size());
        //xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
        xAxis.setTextColor(Color.BLACK);
       // xAxis.mLabelWidth = 10;

        xAxis.setCenterAxisLabels(true);
//        xAxis.setSpaceMax(0.5f * (bd.getGroupWidth(0.3f,0f)) );
        xAxis.setSpaceMin(1f+2*bd.getBarWidth());
        xAxis.setSpaceMax(1f+2*bd.getBarWidth());


        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                int intValue = (int) value;
                Log.i("OAM",intValue+"");
                if (xVals.size() > intValue && intValue >= 0) return xVals.get(intValue);

                return "";
            }
        });

        _cc.getLegend().setTextColor(Color.BLACK);
        _cc.getXAxis().setCenterAxisLabels(true);
        _cc.invalidate();



    }

    private List<Float> getAverage(List<SingleSeries> first,List<SingleSeries> second){
        Log.i("OAM","getAverage");
        List<Float> _average=new ArrayList<>();
        for(int j=0;j<first.size();j++){
         _average.add((first.get(j).get_data()+second.get(j).get_data())/2);
        }
        return _average;
    }

    private LineDataSet GenerateLineDataSet(List<Float> list){
        Log.i("OAM","GenerateLineDataSet");
        List<Entry> _lineEntry=new ArrayList<>();
        int i=0;
        for(Float _each:list){
            _lineEntry.add(new Entry(1f+(i*2f),_each));
            i++;
        }

        LineDataSet set = new LineDataSet(_lineEntry,"Average");

        set.setColor(Color.RED);
        set.setValueTextColor(Color.BLUE);
        set.setValueTextSize(10f);
        set.setLineWidth(3f);
        set.setCircleColor(Color.RED);
        set.setCircleRadius(5f);
        set.setFillColor(Color.RED);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(true);
        set.setValueTextSize(0f);

        set.setAxisDependency(YAxis.AxisDependency.LEFT);


        return set;

    }

    private BarDataSet GenerateBarDataSet(List<SingleSeries> list,boolean isDry){
        Log.i("OAM","GenerateBarDataSet");
        List<BarEntry> _barEntry=new ArrayList<>();
        int i=0;
        for(SingleSeries _series:list){
            _barEntry.add(new BarEntry(i*2.5f,_series.get_data()));
            i++;
        }

        BarDataSet set = new BarDataSet(_barEntry, isDry?"Dry class":"Aqua class");

        set.setColor(isDry? Color.parseColor("#4d47ef"):Color.parseColor("#908cf2"));
        set.setValueTextColor(Color.BLACK);
        set.setValueTextSize(10f);
        set.setBarBorderWidth(1.0f);
        set.setHighLightColor(isDry? Color.parseColor("#4d47ff"):Color.parseColor("#908cf6"));

        return set;
    }



}
