package com.example.rahul.aqua_dashboard;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class RowActivity extends AppCompatActivity {

    enum DatePickerType {
        StartDatePickerType,
        EndDatePickerType
    }

    private RowActivity.DatePickerType selectedDatePicker = RowActivity.DatePickerType.StartDatePickerType;
    private Calendar calendar;
    private EditText startDate;
    private EditText endDate;
    private Button updateReport;


    private void updateDateLabels() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.US);
        if (selectedDatePicker == RowActivity.DatePickerType.StartDatePickerType) {
            startDate.setText(dateFormat.format(calendar.getTime()));
        }
        else {
            endDate.setText(dateFormat.format(calendar.getTime()));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_row);
        configureDateInterface();
        DisplayAqua();

    }

    private void configureDateInterface() {

        calendar        = Calendar.getInstance();
        startDate       = findViewById(R.id.wetstartDate);
        endDate         = findViewById(R.id.wetendDate);
        updateReport    = findViewById(R.id.wetupdateReport);

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            updateDateLabels();
        };


        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                selectedDatePicker = RowActivity.DatePickerType.StartDatePickerType;
                new DatePickerDialog(RowActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                selectedDatePicker = RowActivity.DatePickerType.EndDatePickerType;
                new DatePickerDialog(RowActivity.this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



        updateReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //Date start = new SimpleDateFormat("MM/dd/yy").parse(startDate.getText().toString());
                    DisplayAqua(startDate.getText().toString(),endDate.getText().toString());

                }
                catch(Exception ex){


                }
            }
        });
    }

    void DisplayAqua(String... _dates){

        List<SingleSeries> _details;

        if(_dates.length>0)
         _details=  Data.LoadClass(false,_dates[0],_dates[1]);
        else
            _details=Data.LoadClass(false);

        List<BarEntry> _be=new ArrayList<>();
        int x=0;

        for(SingleSeries e : _details){

            _be.add(new BarEntry(x,e.get_data(),e.get_label()));
            x++;

        }

        BarDataSet _bds=new BarDataSet(_be,"");
        _bds.setColor(Color.parseColor("#4d47ef"));
        _bds.setBarShadowColor(Color.BLACK);
        _bds.setValueTextColor(Color.BLACK);
        _bds.setForm(Legend.LegendForm.NONE);
        _bds.setHighlightEnabled(true);


        BarChart chart=findViewById(R.id.horibc);
        //chart.setNoDataTextColor(R.color.white);
        chart.setDrawGridBackground(false);

        chart.getAxisLeft().setTextColor(Color.BLACK);
        chart.getXAxis().setTextColor(Color.BLACK);

        chart.getXAxis().setDrawGridLines(false);
        chart.getAxisLeft().setDrawGridLines(false);

        XAxis _xaxis = chart.getXAxis();
        _xaxis.setPosition(XAxis.XAxisPosition.TOP_INSIDE);
        _xaxis.setCenterAxisLabels(false);

        _xaxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return _details.get((int)value).get_label();
            }
        });

        chart.getLegend().setTextColor(Color.WHITE);

        BarData _data=new BarData(_bds);
        _data.setBarWidth(0.3f);

        chart.setData(_data);
        chart.animateY(2000);
        chart.invalidate();


    }
}
