package com.example.rahul.aqua_dashboard;

import android.os.Build;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Rahul on 09-06-2018.
 */

public class Data {

    private static final String DEFAULT_DRIVER ="oracle.jdbc.driver.OracleDriver";
    private static final String DEFAULT_URL="jdbc:oracle:thin:@info706.cwwvo42siq12.ap-southeast-2.rds.amazonaws.com:1521:ORCL";
    private static final String DEFAULT_USERNAME="18455242";
    private static final String DEFAULT_PASSWORD="HAMILTON";
    private static final String TAG="OAM";

    private static List<SingleSeries> comparisonChart = null;
    private static List<SingleSeries> dryClass = null;
    private static List<MultipleSeries> monthlyReport = null;

    public Data(){

//        new Thread(() -> {
//            comparisonChart = LoadComparisonChart();
//        }).start();
//        new Thread(() -> {
//            dryClass = LoadDryClass();
//        }).start();
//        new Thread(() -> {
//            monthlyReport = LoadMonthlyReport();
//        }).start();
    }



    //Home count
    protected  static  List<SingleSeries> MemberCount(){

       return SingleSeriesQuery("select cc_name,sum(ct_amount) amount from class_category CC,aqua_class AC,class_timetabled CT " +
                      "where CC.cc_id = AC.ac_cc_id and AC.AC_ID=CT.CT_AC_ID group by cc_name order by cc_name");
    }



    //Overall comparison - piechart
    protected static List<SingleSeries> LoadComparisonChart(PieOperation _pieval){

        String query=null;
        switch(_pieval.ordinal()){
            case 0: query="select cc_name,count(ac_id) from class_category CC,aqua_class AC " +
                    "where CC.cc_id = AC.ac_cc_id group by cc_name ";
                   break;
            case 1:query="select * from (select SUBSTR(ac_name,INSTR(ac_name,'M')+1) Class,count(ac_id) Members from class_category CC,aqua_class AC where CC.cc_id = AC.ac_cc_id and ac_cc_id=1 group by SUBSTR(ac_name,INSTR(ac_name,'M')+1) order by count(ac_id) desc ) where rownum<=5";
                   break; //Dry top classes

            case 2:query="select * from (select SUBSTR(ac_name,INSTR(ac_name,'M')+1) Class,count(ac_id) Members from class_category CC,aqua_class AC where CC.cc_id = AC.ac_cc_id and ac_cc_id=2 group by SUBSTR(ac_name,INSTR(ac_name,'M')+1) order by count(ac_id) desc ) where rownum<=5";
                   break; //Aqua top classes


        }

        return SingleSeriesQuery(query);
    }


    //Dry class details top 5
    protected static List<SingleSeries> LoadClass(boolean isDry,String... _dates){

        String query="select * from (select LTRIM(SUBSTR(ac_name,INSTR(ac_name,'M')+1)) class,sum(ct_amount) from class_category CC,aqua_class AC,class_timetabled CT " +
                "where CC.cc_id = AC.ac_cc_id and AC.AC_ID=CT.CT_AC_ID and CC.cc_id="+(isDry?"1":"2");

        if(_dates.length > 0)
         query+=String.format("and CT.ct_date BETWEEN TO_DATE('%s','MM/dd/yy') AND TO_DATE('%s','MM/dd/yy') ",_dates[0],_dates[1]);

        query+=String.format("group by SUBSTR(ac_name,INSTR(ac_name,'M')+1) order by sum(ct_amount) %s) where rownum<=5",(isDry?"desc":"asc"));
        return SingleSeriesQuery(query);


    }



    //Monthly Report
    protected static List<MultipleSeries> LoadMonthlyReport(List<String> months){

        String _where="";
        if(months.size()>0 && months.size()<12)
        {
            for(String month:months)
                _where+=month+",";

            _where="and extract(month from ct_date) in ("+_where.substring(0,_where.length()-1)+")";
        }


            //1-Dry ,2- Aqua
            Log.i(TAG, "LoadMonthlyReport Entered");
            String query = "select CC.cc_id,to_char(ct_date,'Month'),sum(ct_amount) from class_category CC,aqua_class AC,class_timetabled CT " +
                    "where CC.cc_id = AC.ac_cc_id and AC.AC_ID=CT.CT_AC_ID %s group by CC.cc_id,to_char(ct_date,'Month'),extract(month from ct_date) " +
                    "order by CC.cc_id,extract(month from ct_date)";

            query=String.format(query,_where);
            
            List<MultipleSeries> _multi = new ArrayList<>();
            try {

                List<SingleSeries> _first = new ArrayList<>(), _second = new ArrayList<>();
                for (List<String> _outer : ExecuteQuery(query)) {
                    Integer _categoryId = Integer.parseInt(_outer.get(0));
                    if (_categoryId == 1)
                        _first.add(new SingleSeries(_outer.get(1), Integer.parseInt(_outer.get(2))));
                    else if (_categoryId == 2)
                        _second.add(new SingleSeries(_outer.get(1), Integer.parseInt(_outer.get(2))));

                    _multi.add(new MultipleSeries(_first));
                    _multi.add(new MultipleSeries(_second));

                }

            } catch (Exception ex) {
                Log.i(TAG, "Error in LoadMonthlyReport: " + ex.toString());
            }
            monthlyReport = _multi;


        return monthlyReport;
    }

    public static String NextClassSchedule(){

        String _return=null;

        List<String> schedules =   SingleSeriesQuery("select ac_name,1 from aqua_class").stream().map(x->x.get_label()).collect(Collectors.toList());

        try {
            DateFormat _formatter = new SimpleDateFormat("h:mm a");
            Date current = _formatter.parse(_formatter.format(new Date()));

            Optional _result = schedules.stream().filter(x -> {
                Boolean _returnValue = false;

                String _time = x.substring(0, x.indexOf('M') + 1);
                try {
                    Date parsetime = _formatter.parse(_time);
                    long diff = parsetime.getTime() - current.getTime();
                    if (TimeUnit.MILLISECONDS.toMinutes(diff) > 0)
                        _returnValue = true;

                } catch (Exception ex) {

                }
                return _returnValue;

            }).findFirst();

            if (_result.isPresent()) {
                String _val = _result.get().toString();
                _return = _val.substring(_val.indexOf('M') + 2)+" in ";

                Date parsetime= _formatter.parse(_val.substring(0,_val.indexOf('M')+1));
                long seconds =(parsetime.getTime()-current.getTime())/1000;


                long hours = TimeUnit.SECONDS.toHours(seconds);
                seconds -= TimeUnit.HOURS.toSeconds (hours);

                long minutes = TimeUnit.SECONDS.toMinutes (seconds);
                seconds -= TimeUnit.MINUTES.toSeconds (minutes);

                if(hours>0)
                _return+=String.format("%d hours: ",hours);

                _return+=String.format("%d minutes",minutes);

            }
            else
                _return="Tomorrow";

        }
        catch(Exception ex) {

        }
   return _return;

    }

    private static List<SingleSeries> SingleSeriesQuery(String query){

        Log.i(TAG,"SingleSeriesQuery Entered");
        List<SingleSeries> _list=new ArrayList<>();

       try {

           for(List<String> _outer: ExecuteQuery(query))
           {
               _list.add(new SingleSeries(_outer.get(0),Float.parseFloat(_outer.get(1)) ));
           }

       }
       catch(Exception ex)
       {
       Log.i(TAG,"Error in SingleSeriesQuery Fn");
       }

       return _list;

    }


    private static List<List<String>> ExecuteQuery(String query){


        List<List<String>> _values= new ArrayList<>();
        try{

            StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            Class.forName(DEFAULT_DRIVER);
            Connection con = DriverManager.getConnection(DEFAULT_URL,DEFAULT_USERNAME,DEFAULT_PASSWORD);
            Log.i(TAG,"connected");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()){
                int i=1;
                int columns = rs.getMetaData().getColumnCount();
                ArrayList<String> _subvalues = new ArrayList<String>(columns);

                while(i<=columns)
                    _subvalues.add(rs.getString(i++));

                _values.add(_subvalues);
            }

            con.close();
        }
        catch(Exception ex){

            Log.i(TAG,"Error : "+ex.toString());
        }

        return _values;

    }
}



