package com.example.rahul.aqua_dashboard;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TimingLogger;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.List;
import java.util.stream.Collectors;

public class OverallActivity extends AppCompatActivity {

    private static PieData data = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overall);

        BindSpinner();
        LoadData(PieOperation.All);
    }

    private void BindSpinner(){

       String[] items= new String[]{"All","Dry","Wet"};
           Spinner _spinner= findViewById(R.id.spinner);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line,items);

        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        _spinner.setAdapter(adapter);

        _spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0: LoadData(PieOperation.All); break;
                    case 1: LoadData(PieOperation.Dry); break;
                    case 2: LoadData(PieOperation.Wet); break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    private void LoadData(PieOperation _pieVal){

        //TimingLogger timings = new TimingLogger("ExecTime: ", "list_singleSeries");
        // timings.dumpToLog();
//        new Thread(() -> {
        List<SingleSeries> _classes= Data.LoadComparisonChart(_pieVal);

        List<PieEntry> _pieEntries= _classes.stream().map(f-> new PieEntry(f.get_data(),f.get_label())).collect(Collectors.toList());

        //timings.addSplit("pieDataSet");

        //format chart
        PieDataSet pds=new PieDataSet(_pieEntries,"Comparison");
        pds.setColors(ColorTemplate.createColors(new int[]{Color.parseColor("#262693"),
                Color.parseColor("#101049"),
                Color.parseColor("#332f99"),
                Color.parseColor("#7d7abc"),
                Color.parseColor("#a9a9ce")
        }));

        pds.setValueTextSize(25f);
        pds.setValueTextColor(Color.WHITE);
        pds.setValueFormatter(new PercentFormatter());

        pds.setSliceSpace(10f);  //Gap between pieces
        pds.setSelectionShift(15f);
        pds.setYValuePosition(PieDataSet.ValuePosition.INSIDE_SLICE);

      //  timings.addSplit("pieData");

        if (OverallActivity.data != null)
           OverallActivity.data.clearValues();

            OverallActivity.data = new PieData(pds);


//       }).start();


        PieChartComparison(OverallActivity.data);
    }



    //Overall glimpse
    private void PieChartComparison(PieData data){

            try {

                    PieChart _pc = findViewById(R.id.pcCompare);
                   _pc.invalidate();
                   _pc.clear();

                    _pc.getLegend().setTextColor(Color.WHITE);
                    _pc.setUsePercentValues(true);
                    _pc.setDrawHoleEnabled(true);
                    _pc.setRotationEnabled(true);
                    _pc.setHoleRadius(35);
                    _pc.setHoleColor(Color.BLACK);
                    _pc.getDescription().setEnabled(true);
                    // _pc.setMaxAngle(180);
                    _pc.setRotationAngle(180f);
                    _pc.setData(data);
                    _pc.animateY(2000); //animate chart on displaying
                    _pc.invalidate();

            }
            catch (Exception e) {
                //print the error here
            }

    }
}
